package calculadora.presentation;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author joao_
 */
public class Calculadora implements ActionListener{
    
    JFrame frame;
    JTextField campoTexto;
    JButton[] botoesNumeros = new JButton[10];
    JButton[] botoesOperacoes = new JButton[9];
    JButton btSoma, btSubtrai, btMulti, btDivid;
    JButton btDecimal, btIgual, btDel, btApaga, btNegativo;
    JPanel painelCalc;
    
    Font letra = new Font("Arial", Font.BOLD, 30);
    
    double num1=0, num2=0, resultado=0;
    char operador;
    
    public Calculadora(){
        
        frame = new JFrame("Calculadora");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(420,550);
        frame.setLayout(null);
        
        campoTexto = new JTextField();
        campoTexto.setBounds(50, 25, 300, 50);
        campoTexto.setFont(letra);
        campoTexto.setEditable(false);
        
        btSoma = new JButton("+");
        btSubtrai = new JButton("-");
        btMulti = new JButton("X");
        btDivid = new JButton("÷");
        btDecimal = new JButton(".");
        btIgual = new JButton("=");
        btDel = new JButton("DEL");
        btApaga = new JButton("AC");
        btNegativo = new JButton("(-)");
        
        botoesOperacoes[0] = btSoma;
        botoesOperacoes[1] = btSubtrai;
        botoesOperacoes[2] = btMulti;
        botoesOperacoes[3] = btDivid;
        botoesOperacoes[4] = btDecimal;
        botoesOperacoes[5] = btIgual;
        botoesOperacoes[6] = btDel;
        botoesOperacoes[7] = btApaga;
        botoesOperacoes[8] = btNegativo;
        
        for(int i = 0; i<9; i++){
            botoesOperacoes[i].addActionListener(this);
            botoesOperacoes[i].setFont(letra);
            botoesOperacoes[i].setFocusable(false);
        }
        
        for(int i = 0; i<10; i++){
            botoesNumeros[i] = new JButton(String.valueOf(i));
            botoesNumeros[i].addActionListener(this);
            botoesNumeros[i].setFont(letra);
            botoesNumeros[i].setFocusable(false);
            
        }
        
        btNegativo.setBounds(50, 430, 100, 50);
        btDel.setBounds(150, 430, 100, 50);
        btApaga.setBounds(250, 430, 100, 50);
        
        painelCalc = new JPanel();
        painelCalc.setBounds(50, 100, 300, 300);
        painelCalc.setLayout(new GridLayout(4, 4, 10, 10));
        //ver a cor: painelCalc.setBackground(Color.GRAY);
        
        painelCalc.add(botoesNumeros[1]);
        painelCalc.add(botoesNumeros[2]);
        painelCalc.add(botoesNumeros[3]);
        painelCalc.add(btSoma);
        painelCalc.add(botoesNumeros[4]);
        painelCalc.add(botoesNumeros[5]);
        painelCalc.add(botoesNumeros[6]);
        painelCalc.add(btSubtrai);
        painelCalc.add(botoesNumeros[7]);
        painelCalc.add(botoesNumeros[8]);
        painelCalc.add(botoesNumeros[9]);
        painelCalc.add(btMulti);
        painelCalc.add(btDecimal);
        painelCalc.add(botoesNumeros[0]);
        painelCalc.add(btIgual);
        painelCalc.add(btDivid);
        
        frame.add(painelCalc);
        frame.add(btNegativo);
        frame.add(btDel);
        frame.add(btApaga);
        frame.add(campoTexto);
        frame.setVisible(true);
                
    }
    public static void main(String[] args) {
        
        Calculadora calc = new Calculadora();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        for(int i = 0; i<10; i++){
            if(ae.getSource() == botoesNumeros[i]){
                campoTexto.setText(campoTexto.getText().concat(String.valueOf(i)));
            }
        }
        if(ae.getSource() == btDecimal){
            campoTexto.setText(campoTexto.getText().concat("."));
        }
        if(ae.getSource() == btSoma){
            num1 = Double.parseDouble(campoTexto.getText());
            operador = '+';
            campoTexto.setText("");
        }
        if(ae.getSource() == btSubtrai){
            num1 = Double.parseDouble(campoTexto.getText());
            operador = '-';
            campoTexto.setText("");
        }
        if(ae.getSource() == btMulti){
            num1 = Double.parseDouble(campoTexto.getText());
            operador = '*';
            campoTexto.setText("");
        }
        if(ae.getSource() == btDivid){
            num1 = Double.parseDouble(campoTexto.getText());
            operador = '/';
            campoTexto.setText("");
        }
        if(ae.getSource() == btIgual){
            num2 = Double.parseDouble(campoTexto.getText());
            
            switch(operador){
                case '+':
                    resultado = num1 + num2;
                    break;
                case '-':
                    resultado = num1 - num2;
                    break;
                case '*':
                    resultado = num1 * num2;
                    break;
                case '/':
                    resultado = num1 / num2;
                    break;
            }
            campoTexto.setText(String.valueOf(resultado));
            num1 = resultado;
        }
        if(ae.getSource() == btApaga){
            campoTexto.setText("");
        }
        if(ae.getSource() == btDel){
            String string = campoTexto.getText();
            campoTexto.setText("");
            for(int i=0; i<string.length()-1; i++){
                campoTexto.setText(campoTexto.getText()+string.charAt(i));
            }
        }
        if(ae.getSource() == btNegativo){
            double temp = Double.parseDouble(campoTexto.getText());
            temp*=-1;
            campoTexto.setText(String.valueOf(temp));
        }
    }
}